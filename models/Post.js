// User model for database
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//External model for make dipendences
var Post = new Schema({
    date: {
        type: Date,
        default: new Date()
    },
    update: {
        type: Date,
        default: new Date()
    },
    socialID: {
        type: String
    },
    publishedAt: {
        type: Date
    },
    UserShort: {
        image: {
            type: String
        },
        name: {
            type: String
        },
        socialID: {
            type: String
        }
    },
    image: {
        type: String
    },
    postMessage: {
        type: String
    },
    socials: {
        provider: {
            type: String
        },
        user: {
            type: Object
        }
    },
    otherSocialInfo: {
        type: Object
    }
});

exports.Post = Post;
