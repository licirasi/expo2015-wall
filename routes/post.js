var log = require('../utils/log.js');

exports.getAll = function (req, res) {
    var page = req.param("page");
    var itemforpage = req.param("itemforpage");
    if (Number(page) && Number(itemforpage)) {
        page = (page < 1) ? 1 : page;
        var realpage = itemforpage * (page - 1);
        req.db.post.find({}).skip(Number(realpage)).limit(Number(itemforpage)).sort({
            'publishedAt': -1
        }).exec(function (err, posts) {
            if (err) {
                log.error.api("[wall] get all " + err);
                return res.staus(500).json({
                    message: "sorry internal server error",
                    err: JSON.stringify(err)
                });
            } else {
                log.success.api("[wall] get all " + posts.length);
                page++;
                return res.status(200).json({
                    data: posts,
                    next: "/api/v1/wall/" + page + "/" + itemforpage
                });
            }
        })
    } else {
        req.db.post.find({}).limit(20).sort({
            'publishedAt': -1
        }).exec(function (err, posts) {
            if (err) {
                log.error.api("[wall] get all " + err);
                return res.staus(500).json({
                    message: "sorry internal server error",
                    err: JSON.stringify(err)
                });
            } else {
                log.success.api("[wall] get all " + posts.length);
                return res.status(200).json({
                    data: posts,
                    next: "/api/v1/wall/2/20"
                });
            }
        })
    }
}
