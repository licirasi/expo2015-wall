var graph = require('fbgraph'),
    FB = require('fb'),
    app = require('express')(),
    log = require('../utils/log.js'),
    configuration = require("../config/config.json");
var db = require('./database').db,
    Q = require('q'),
    http = require('http');
var searchOptions = {
    q: configuration.tag,
    type: "post"
};
var arrayProm = [];

function _init(functionOnSetting) {
    FB.api('oauth/access_token', configuration.FACEBOOK[app.get('env')], function (res) {
        if (!res || res.error) {
            log.error.api(!res ? 'error occurred' : JSON.stringify(res.error));
            return;
        }
        log.success.api("Required accessToken from Facebook");
        var accessToken = res.access_token;
        graph.setAccessToken(accessToken);
        log.success.api("Setted accessToken from Facebook");
        functionOnSetting();
    });
}

function _search() {
    graph.search(searchOptions, function (err, res) {
        if (err) {
            log.error.api("[Facebook] data: " + JSON.stringify(err));
            return;
        }
        log.success.api("[Facebook] data: " + res.data.length);
        for (var a in res.data) {
            _saveInDb(res.data[a]);
        }

    });
}


function _saveInDb(us) {
    db.post.find({
        socialID: us.id
    }).exec(function (err, var_db) {
        if (err) {
            return;
        }
        if (var_db.length > 0) {
            log.error.database("[Facebook] just exist");
            return;
        } else {
            var Post = new db.post({
                date: new Date(),
                update: new Date(),
                socialID: us.id,
                publishedAt: new Date(us.created_time),
                UserShort: {
                    image: "",
                    name: us.from.name,
                    socialID: us.from.id
                },
                image: us.picture,
                postMessage: us.message,
                socials: {
                    provider: "facebook",
                    user: us.from
                },
                otherSocialInfo: {
                    link: us.link,
                    user_tagged: us.to
                }
            });
            Post.save(function (err) {
                if (err) log.error.database("[Facebook] err: " + err);
                else log.success.database("[Facebook] saved: " + Post.socialID);
            });
        }
    });
}


function _searchAndScrol(prev) {
    var conf = configuration.FACEBOOK[app.get('env')];
    conf.fb_exchange_token = graph.getAccessToken();
    configuration.FACEBOOK[app.get('env')].fb_exchange_token = graph.getAccessToken();
    FB.api('oauth/access_token', configuration.FACEBOOK[app.get('env')], function (res) {
        if (!res || res.error) {
            log.error.api(!res ? 'error occurred' : JSON.stringify(res.error));
            return;
        }
        log.success.api("Setted accessToken from Facebook");
        var accessToken = res.access_token;
        graph.setAccessToken(accessToken);
        if (!prev) {
            graph.search(searchOptions, function (err, res) {
                if (err) {
                    log.error.api("[Facebook] data: " + JSON.stringify(err));
                    return;
                }
                graph.search(searchOptions, function (err, res) {
                    if (err) {
                        log.error.api("[Facebook] data: " + JSON.stringify(err));
                        return;
                    }
                    log.success.api("[Facebook] data: " + res.data.length);
                    Q().then(function () {
                        var arrayProm = [];
                        for (var i = 0; i < res.data.length; i++) {
                            arrayProm.push(_saveInDbSincred(res.data[i]));
                        }
                        return arrayProm;
                    }).all().then(function (promises) {
                        var conti = true;
                        for (var n in promises) {
                            if (promises[n].continue == false) {
                                conti = false
                            }
                        }
                        if (conti) {
                            //                            _searchAndScrol(res.paging.next);
                            _searchAndScrol();
                        } else {
                            log.success.api("[Facebook] database updated ");
                        }
                    });
                });

            });
        } else {
            var request = require('request');
            request(prev, function (error, res, body) {
                if (!error && res.statusCode == 200) {
                    log.success.api("[Facebook] data: " + res.data.length);
                    Q().then(function () {
                        var arrayProm = [];
                        for (var i = 0; i < res.data.length; i++) {
                            arrayProm.push(_saveInDbSincred(res.data[i]));
                        }
                        return arrayProm;
                    }).all().then(function (promises) {
                        var conti = true;
                        for (var n in promises) {
                            if (promises[n].continue == false) {
                                conti = false
                            }
                        }
                        if (conti) {
                            _searchAndScrol(res.paging.next);
                        } else {
                            log.success.api("[Facebook] database updated ");
                        }
                    });
                } else {
                    log.error.api("[Facebook] data: " + JSON.stringify(error));
                    return;
                }
            })
        }
    });
};


function _saveInDbSincred(us) {
    var deferred = Q.defer();
    db.post.find({
        socialID: us.id
    }).exec(function (err, var_db) {
        if (err) {
            return;
        }
        if (var_db.length > 0) {
            log.error.database("[Facebook] just exist");
            deferred.resolve({
                continue: false
            });
        } else {
            var Post = new db.post({
                date: new Date(),
                update: new Date(),
                socialID: us.id,
                publishedAt: new Date(us.created_time),
                UserShort: {
                    image: "",
                    name: us.from.name,
                    socialID: us.from.id
                },
                image: us.picture,
                postMessage: us.message,
                socials: {
                    provider: "facebook",
                    user: us.from
                },
                otherSocialInfo: {
                    link: us.link,
                    user_tagged: us.to
                }
            });
            Post.save(function (err) {
                if (err) {
                    log.error.database("[Facebook] err: " + err);
                    return deferred.reject();
                } else {
                    log.success.database("[Facebook] saved: " + Post.socialID);
                    deferred.resolve({
                        continue: true
                    });
                }
            });
        }
    });
    return deferred.promise;
}


exports.init = _init;
exports.search = _search;
exports.searchPromesis = _searchAndScrol;