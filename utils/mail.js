var nodemailer = require('nodemailer'),
    configuration = require("../config/config.json"),
    log = require('../utils/log.js'),
    app = require('express')();
var _transporter;

function _init() {
    if (!configuration.GMAIL[app.get('env')].email || !configuration.GMAIL[app.get('env')].password) return log.error.app('Missing GMAIL Settings');
    // create reusable transporter object using SMTP transport
    _transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: configuration.GMAIL[app.get('env')].email,
            pass: configuration.GMAIL[app.get('env')].password
        }
    });
}
/*
 var mailOptions = {
        from: 'Fred Foo ✔ <foo@blurdybloop.com>', // sender address
        to: 'bar@blurdybloop.com, baz@blurdybloop.com', // list of receivers
        subject: 'Hello ✔', // Subject line
        text: 'Hello world ✔', // plaintext body
        html: '<b>Hello world ✔</b>' // html body
    };

*/
function send(mailOptions, callback) {
    if (!_transporter) return log.error.app('Email need .init()');
    _transporter.sendMail(mailOptions, function (error, info) {
        if (error) log.error.api("[Email] - Email send to " + mailOptions.to);
        else log.success.api("[Email] -  Email send to " + mailOptions.to);
        return callback(error, info)
    });
}

exports.init = _init;
exports.send = send;
