 var configuration = require("../config/config.json"),
     apnagent = require('apnagent'),
     fs = require('fs'),
     join = require('path').join,
     log = require('../utils/log.js'),
     app = require('express')(),
     gcm = require('node-gcm');

 var sender;

 exports.init = function () {
     if (!configuration.PUSH[app.get('env')].android.apiKey) return log.error.app('Missing ANDROID GCM Push key');
     _sender = new gcm.Sender(configuration.PUSH[app.get('env')].android.apiKey);
     if (_sender) log.success.app("[ANDROID-PUSH] gcm connected");
 };
 //{
 //        collapseKey: 'demo',
 //        delayWhileIdle: true,
 //        timeToLive: 3,
 //          dryRun : false,
 //        data: {
 //            key1: 'message1',
 //            key2: 'message2'
 //        }
 //    }
 // obj, obj, array
 exports.sender = {
     send: function pushAndroid(message, data, registrationIds, callback) {
          if (!sender) return log.error.app('Push need .init()');
         var message = new gcm.Message(message);
         message.addData(data)
         // Send the message
         sender.sendNoRetry(message, registrationIds, function (err, result) {
             if (err) return callback(err, null);
             else return callback(null, result);
         });
     }
 }
