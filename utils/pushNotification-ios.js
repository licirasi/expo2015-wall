var configuration = require("../config/config.json"),
    apnagent = require('apnagent'),
    fs = require('fs'),
    join = require('path').join,
    log = require('../utils/log.js'),
    app = require('express')();
var pfx = join(__dirname, configuration.PUSH[app.get('env')].ios.file_name);
var _agent;
var _init = function () {
    try {
        fs.readFileSync(pfx);
    } catch (e) {
        return log.error.app('Missing IOS Push certificates');
    }
    _agent = new apnagent.Agent();
    _agent.set('pfx file', pfx).set('passphrase', configuration.PUSH[app.get('env')].ios.password_p12);
    if ('production' == app.get('env')) {
        _agent.enable();
    } else {
        _agent.enable('sandbox');
    }
    _agent.connect(function (err) {
        // gracefully handle auth problems
        if (err) {
            return log.error.app("[APPLE-PUSH] Authentication Error", {
                type: err.name,
                message: err.message
            });
        }
        var _en = applePushAgent.enabled('sandbox') ? 'sandbox' : 'production';
        log.success.app("[APPLE-PUSH] Authentication with apnagent [" + _en + "] gateway connected");
        return applePushAgent;
    });
    _agent.on('message:error', function (err, msg) {
        switch (err.name) {
            // This error occurs when Apple reports an issue parsing the message.
        case 'GatewayNotificationError':
            log.error.app('[message:error] GatewayNotificationError:' + err.message);
            // The err.code is the number that Apple reports.
            // Example: 8 means the token supplied is invalid or not subscribed
            // to notifications for your application.
            if (err.code === 8) {
                log.error.app('    >' + msg.device().toString());
                // In production you should flag this token as invalid and not
                // send any futher messages to it until you confirm validity
            }
            break;
            // This happens when apnagent has a problem encoding the message for transfer
        case 'SerializationError':
            log.error.app('[message:error] SerializationError:' + err.message);
            break;
            // unlikely, but could occur if trying to send over a dead socket
        default:
            log.error.app('[message:error] other error:' + err.message);
            break;
        }
    });
}
exports.init = _init;
exports.agent = function () {
    if (!_agent) {
        log.error.app('Push need .init()');
        return null;
    }
    return _agent;
};
/*
send example
exports.send = function (conf) {
    _agent.createMessage('bc96b3f9aa584df3db75419c92c4e571b4c7f588ba05179d596849978292d232')
        .badge(999999999999)
        .sound("PushNotification.caf")
        .device("bc96b3f9aa584df3db75419c92c4e571b4c7f588ba05179d596849978292d232")
        .alert("Test message")
        .set('id', '001')
        .send(function (err) {
            // Error
            if (err) return res.send(500)
            return res.send(200);
        });
});
};
*/
