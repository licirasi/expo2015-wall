var yt = require('youtube-node'),
    app = require('express')(),
    log = require('../utils/log.js'),
    configuration = require("../config/config.json");
var db = require('./database').db;
var youTube = new yt();

function _init() {
    youTube.setKey('AIzaSyB1OOSpTREs85WUMvIgJvLTZKye4BVsoFU');
}

function _search() {
    youTube.search(configuration.hashtag, 20, function (resultData) {
        if (resultData.errors) {
            log.error.api("[Youtube] err: " + resultData.errors);
        } else {
            log.success.api("[Youtube] data: " + resultData.items.length);
            for (var a in resultData.items) {
                _saveInDb(resultData.items[a]);
            }
        }
    });
}

function _searchAndScrol() {
    youTube.search(configuration.hashtag, 50, function (resultData) {
        if (resultData.errors) {
            log.error.api("[Youtube] err: " + JSON.stringify(resultData.errors));
        } else {
            log.success.api("[Youtube] data: " + resultData.items.length);
            for (var a in resultData.items) {
                _saveInDb(resultData.items[a]);
            }
        }
    });
}

function _saveInDb(us) {
    db.post.find({
        socialID: us.id.videoId
    }).exec(function (err, var_db) {
        if (err) {
            return;
        }
        if (var_db.length > 0) {
            log.error.database("[Youtube] just exist");
            return;
        } else {
            var Post = new db.post({
                date: new Date(),
                update: new Date(),
                socialID: us.id.videoId,
                publishedAt: new Date(us.snippet.publishedAt),
                UserShort: {
                    image: "",
                    name: us.snippet.channelTitle,
                    socialID: us.snippet.channelId
                },
                image: us.snippet.thumbnails.medium.url,
                postMessage: us.snippet.title,
                socials: {
                    provider: "youtube",
                    user: us.snippet.channelId
                },
                otherSocialInfo: {
                    snippet: us.snippet,
                    kind: us.kind
                }
            });
            Post.save(function (err) {
                if (err) log.error.database("[Youtube] err: " + err);
                else log.success.database("[Youtube] saved: " + Post.socialID);
            });
        }
    });
}

exports.searchPromesis = _searchAndScrol;
exports.init = _init;
exports.search = _search;